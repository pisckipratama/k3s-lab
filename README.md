# K3s Lab

This repository is my documentation for building a kubernetes cluster using K3s.

Link to official site [K3s](https://k3s.io) or [K3d](https://k3d.io) for the dockerized version of k3s.


## Getting started

Make sure you have 2 VMs Ubuntu Server 20.04 or later with 1 core CPU and 2 GB RAM. You can use VirtualBox for those VMs.

## Build Master/Control Plane Node
`curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION="v1.24.7+k3s1" sh -s - --no-deploy traefik --write-kubeconfig-mode 644 --node-name k3s-master`

Parameter Explanation:
- install with v1.24 version with k3s version
- no deploy traefik, because we will use nginx for ingress controller
- write kubeconfig to get kubectl command and config cluster
- define k3s-master for the node name

## Test Control Plane Node
Get nodes command with other information:

`kubectl get nodes -o wide`

Get all of pods in the cluster:

`kubectl get pods -A`

## Install and Register Worker Node
Grab token from the master node to be able to add worked nodes to it: 

`cat /var/lib/rancher/k3s/server/node-token`

Install k3s on the worker node and add it to our cluster:

`curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION="v1.24.7+k3s1" K3S_NODE_NAME=k3s-worker K3S_URL=https://<IP>:6443 K3S_TOKEN=<TOKEN> sh - `

## Install Nginx Ingress Controller
`kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.2.1/deploy/static/provider/cloud/deploy.yaml`

## Deploy Example App to K3s Cluster
Deploy echo service for first app:

`kubectl apply -f echo-server`

## Testing Deployment
Get pods:

`kubectl get pods`

Check echo-server service:

`curl http://<ip_address>.nip.io` or open on browser

Increase number of pod echo-server:

`kubectl scale deploy echo-server --replicas=3`

Check again to test loadbalancer functional:

`for i in {1..15}; do curl -s http://<ip_address>.nip.io | grep Hostname: ; done`

## (Optional) Deploy Wordpress
`kubectl apply -k wordpress`

using `-k` because the deployment method using [Kustomization](https://kustomize.io/)

Get pod, svc, persistent volume claim, and persistent volume:

`kubectl get pod,svc,pvc,pv`

If the pods are running all, go to your browser and open `http://wordpress.<ip_adress>.nip.io`, and then configure the wordpress admin.